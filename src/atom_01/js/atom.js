function init() {


  var stats = initStats();
  var renderer = initRenderer();
  var camera = initCamera(new THREE.Vector3(0, 20, 40));
  var trackballControls = initTrackballControls(camera, renderer);
  var clock = new THREE.Clock();
  //var speed = 20;
  //var delta = 0;

  var scene = new THREE.Scene();

  initDefaultLighting(scene);
  scene.add(new THREE.AmbientLight(0x444444));

  var video = document.getElementById( 'video' );
  var texture = new THREE.VideoTexture(video);
  texture.minFilter = THREE.LinearFilter;
  texture.magFilter = THREE.LinearFilter;
  texture.format = THREE.RGBFormat;

  var gui = new dat.GUI();
  var controls = {};

  var poly = new THREE.IcosahedronGeometry(4, 0);

  //1.atoms (lieto 2 objektus, lai ņirbuļotu)
  var atom = addGeometry(scene, poly, 'molekula1', texture, gui, controls); // video tiek pielikts caur controls kā texture mainīgais
  var atom2 = addGeometry(scene, poly, 'molekula2', texture, gui, controls);// viens otram virsuu, lai ņirbuļo
  
  var atom3 = addGeometry(scene, poly, 'molekula3', texture, gui, controls);
  atom3.position.x = 10;
  var atom4 = addGeometry(scene, poly, 'molekula4', texture, gui, controls)
  atom4.position.x = 10;

  render();
  function render() {
    stats.update();
    trackballControls.update(clock.getDelta());
    requestAnimationFrame(render);
    //delta = clock.getDelta();
    atom.rotation.x += 0.01;
    atom.position.z += 0.02;
    atom2.rotation.x -= 0.01;
    atom2.position.z += 0.02;

    atom3.rotation.x += 0.01;
    atom3.position.z -= 0.02;
    atom4.rotation.x -= 0.01;
    atom4.position.z -= 0.02;

    renderer.render(scene, camera);
    

  
  }
}
